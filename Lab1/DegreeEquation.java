import javax.swing.JOptionPane;
import java.lang.Math;
public class DegreeEquation{
    public static void main(String[] args){
       String strcoeA,strcoeB;
       String strNotification1,strNotification2,strNotification3;
       strcoeA=JOptionPane.showInputDialog(null,"Input the first coefficient" ,"The first-degree equation with one variable ",JOptionPane.INFORMATION_MESSAGE);
       strcoeB=JOptionPane.showInputDialog(null,"Input the second coefficient","The first-degree equation with one variable ",JOptionPane.INFORMATION_MESSAGE);
       double coeA = Double.parseDouble(strcoeA);
       double coeB = Double.parseDouble(strcoeB);
       
       strNotification1="The equation has a unique solution x= " + -coeB/coeA;
       JOptionPane.showMessageDialog(null,strNotification1,"The solution for first_degree equation", JOptionPane.INFORMATION_MESSAGE);

       String strcoeA11,strcoeA12,strcoeA21,strcoeA22,strcoeB1,strcoeB2;
       strcoeA11=JOptionPane.showInputDialog(null,"Input the a11 coefficient in a11.x1+a12.x2=b1: ","The second-degree equation with one variable ",JOptionPane.INFORMATION_MESSAGE);
       strcoeA12=JOptionPane.showInputDialog(null,"Input the a12 coefficient in a11.x1+a12.x2=b1: ","The second-degree equation with one variable ",JOptionPane.INFORMATION_MESSAGE);
       strcoeB1 =JOptionPane.showInputDialog(null,"Input the b1 coefficient in a11.x1+a12.x2=b1: ","The second-degree equation with one variable ",JOptionPane.INFORMATION_MESSAGE);
       
       strcoeA21=JOptionPane.showInputDialog(null,"Input the a21 coefficient in a21.x1+a22.x2=b2: ","The second-degree equation with one variable ",JOptionPane.INFORMATION_MESSAGE);
       strcoeA22=JOptionPane.showInputDialog(null,"Input the a22 coefficient in a21.x1+a22.x2=b2: ","The second-degree equation with one variable ",JOptionPane.INFORMATION_MESSAGE);
       strcoeB2 =JOptionPane.showInputDialog(null,"Input the b2 coefficient in a21.x1+a22.x2=b2: ","The second-degree equation with one variable ",JOptionPane.INFORMATION_MESSAGE);
       double coeA11 = Double.parseDouble(strcoeA11);
       double coeA12 = Double.parseDouble(strcoeA12);
       double coeB1  = Double.parseDouble(strcoeB1);

       double coeA21 = Double.parseDouble(strcoeA21);
       double coeA22 = Double.parseDouble(strcoeA22);
       double coeB2  = Double.parseDouble(strcoeA22);

       double D = coeA11*coeA22 - coeA12*coeA21;
       double D1= coeB1 *coeA22 - coeB2 *coeA12;
       double D2= coeA11*coeB2  - coeA21*coeB1 ;

       

       if(D!=0){
       strNotification2="The solution x1 = " + D1/D + "\n"
       + "The solution x2 = " + D2/D;
       JOptionPane.showMessageDialog(null,strNotification2,"The solution for second_degree equation", JOptionPane.INFORMATION_MESSAGE);

       }
       else if(D==0){
       strNotification2="The system has infinitely many solutions";
       JOptionPane.showMessageDialog(null,strNotification2,"The solution for second_degree equation", JOptionPane.INFORMATION_MESSAGE);
       }
       else{
       strNotification2="The system has no solution";    
       JOptionPane.showMessageDialog(null,strNotification2,"The solution for second_degree equation", JOptionPane.INFORMATION_MESSAGE);
       }

       String strcoe1,strcoe2,strcoe3;
       strcoe1=JOptionPane.showInputDialog(null,"Input the coefficient a in a.x^2+b.x+c=0: ","The second-degree equation with one variable",JOptionPane.INFORMATION_MESSAGE);
       strcoe2=JOptionPane.showInputDialog(null,"Input the coefficient b in a.x^2+b.x+c=0: ","The second-degree equation with one variable",JOptionPane.INFORMATION_MESSAGE);
       strcoe3=JOptionPane.showInputDialog(null,"Input the coefficient c in a.x^2+b.x+c=0: ","The second-degree equation with one variable",JOptionPane.INFORMATION_MESSAGE);

       Double coe1 = Double.parseDouble(strcoe1);
       Double coe2 = Double.parseDouble(strcoe2);
       Double coe3 = Double.parseDouble(strcoe3);
       Double delta= coe2*coe2 - 4*coe1*coe3;

       if(delta>0){
           strNotification3="The equation has 2 solution x1= "+ (-coe2+Math.sqrt(delta))/(2*coe1)+ "and x2= "+(-coe2-Math.sqrt(delta))/(2*coe1);
           JOptionPane.showMessageDialog(null,strNotification3,"The solution for the second-degree equation with one variable",JOptionPane.INFORMATION_MESSAGE);

       }
       else if(delta==0){
           strNotification3="The equation has double root x1=x2= "+ (-coe2/(2*coe1));
           JOptionPane.showMessageDialog(null,strNotification3,"The solution for the second-degree equation with one variable",JOptionPane.INFORMATION_MESSAGE);
       }
       else{
           JOptionPane.showMessageDialog(null,"The equation has no solution","The solution for the second-degree equation with one variable",JOptionPane.INFORMATION_MESSAGE);
       }
    }
}