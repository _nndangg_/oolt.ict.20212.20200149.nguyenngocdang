import javax.swing.JOptionPane;
public class TwoDouble{
    public static void main(String[] args){
       String strNum1,strNum2;
       String strNotification="You've just entered ";
       strNum1=JOptionPane.showInputDialog(null,"Please input the first number: ","Input the first number",JOptionPane.INFORMATION_MESSAGE);
       double num1 = Double.parseDouble(strNum1);

       strNum2=JOptionPane.showInputDialog(null,"Please input the second number: ","Input the second number",JOptionPane.INFORMATION_MESSAGE);
       double num2 = Double.parseDouble(strNum2);
       double sum=num1+num2;
       double diff=num1-num2;
       double product=num1*num2;
       double quotient=num1/num2;
       strNotification = "The sum: "+ strNum1 + "+" + strNum2 + "=" + sum +"\n" 
       + "The diff: "     + strNum1 + "-" + strNum2 + "=" + diff + "\n"
       + "The product: "  + strNum1 + "x" + strNum2 + "=" + product + "\n"
       + "The quotient: " + strNum1 + ":" + strNum2 + "=" + quotient;
    
       JOptionPane.showMessageDialog(null,strNotification,"Show two numbers", JOptionPane.INFORMATION_MESSAGE);
       System.exit(0);


    }
}