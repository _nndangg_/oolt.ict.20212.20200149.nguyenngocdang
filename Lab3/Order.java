public class Order {
    private int qtyOrdered = 1;
    public static final int MAX_NUMBERS_ORDERED = 10;
    
    private DigitalVideoDisc iteamsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];

    public int getQtyOrdered() {
        return qtyOrdered;
    }
    public void setQtyOrdered(int qtyOrdered) {
      this.qtyOrdered = qtyOrdered;
  }
    public String display(int i) {
        return iteamsOrdered[i].getTitle();
    }
    public void  addDigitalVideoDisc(DigitalVideoDisc disc) {
        if(qtyOrdered<=10) {
            iteamsOrdered[qtyOrdered] = disc;
            qtyOrdered++;
        }
        
    }
    
    public void  removeDigitalVideoDisc(DigitalVideoDisc disc) {
        int i=1;
        while (iteamsOrdered[i]!= disc) i++;
        qtyOrdered--;
        for (int j = i ; j<qtyOrdered ; j++) {
            iteamsOrdered[j] = iteamsOrdered[j+1];
        }
    }
    
    public float totalCost() {
        float cost =0;
        for (int i=1;i<qtyOrdered;i++) {
            cost =cost + iteamsOrdered[i].getCost();
        }
        return cost;
    }
    
}