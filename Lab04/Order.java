import java.time.Instant;

public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	public static final int MAX_LIMITTED_ORDERS = 5;
	
	private DigitalVideoDisc itemOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	public int qtyOrdered = 0;
	private MyDate dateOrdered;
	private static int nbOrders = 0;
	
	public Order ()
	{
		if (nbOrders < MAX_LIMITTED_ORDERS) {
			qtyOrdered = 0;
			dateOrdered = new MyDate();
			nbOrders = nbOrders + 1;
		}
		else {
			System.out.println("Exceed the number of orders!");
			nbOrders = nbOrders + 1;
		}
	}
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if (nbOrders <= MAX_LIMITTED_ORDERS) {
			if (qtyOrdered < MAX_NUMBERS_ORDERED) {
				itemOrdered[qtyOrdered] = disc;
				qtyOrdered = qtyOrdered + 1;
				System.out.print("This disc has been added!\n");
			}
			else System.out.print("The order is almost full!\n");
		}
	}
	
	public void addDigitalVideoDics(DigitalVideoDisc [] dvdList)
	{
		if (nbOrders <= MAX_LIMITTED_ORDERS) {
			if (dvdList.length + qtyOrdered >= MAX_NUMBERS_ORDERED) {
				System.out.print("The list of disc cannot be added because of full ordered items!\n");
			}
			else {
				for (int i=0; i<dvdList.length; i++) {
					itemOrdered[qtyOrdered] = dvdList[i];
					qtyOrdered = qtyOrdered+1;
				}
				System.out.print("The list of disc have been added!\n");
			}
		}
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2)
	{
		if (nbOrders <= MAX_LIMITTED_ORDERS) {
			if (qtyOrdered < MAX_NUMBERS_ORDERED) {
				itemOrdered[qtyOrdered] = dvd1;
				qtyOrdered = qtyOrdered + 1;
				System.out.print("The first dvd has been added!\n");
			}
			else System.out.print("The order is almost full!\n");
			if (qtyOrdered < MAX_NUMBERS_ORDERED) {
				itemOrdered[qtyOrdered] = dvd2;
				qtyOrdered = qtyOrdered + 1;
				System.out.print("The second dvd has been added!\n");
			}
			else System.out.print("The order is almost full!\n");
		}
	}
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		if (nbOrders <= MAX_LIMITTED_ORDERS) {
			if (itemOrdered[qtyOrdered-1] == disc) {
				--qtyOrdered;
				System.out.print("This disc has been removed\n");
				return;
			}
			if (qtyOrdered > 0) {
				for (int i=0; i<qtyOrdered; i++) 
					if (itemOrdered[i] == disc) {
						itemOrdered[i] = itemOrdered[--qtyOrdered]; 
						System.out.print("This disc has been removed\n");
						return;
					}
			}
			System.out.print("This disc does not have in your order\n");
		}
	}
	
	public float totalCost() {
		float sum = 0;
		for (int i=0; i<qtyOrdered; i++ )  {
			sum = sum + itemOrdered[i].getCost();
		}
		return sum;
	}
	
	public void printing() {
		System.out.println("***************************************** ORDER *****************************************");
        System.out.println("Date: " + dateOrdered.getDay() + "/" + dateOrdered.getMonth() + "/" + dateOrdered.getYear() + "\nOrdered Items:");
        System.out.println("  Name - Title - Category - Director - Length : Price($)");
        for(int i=0; i< qtyOrdered; i++) {
            System.out.println((i+1) + ".DVD - " + itemOrdered[i].getTittle() + " - " + itemOrdered[i].getCategory() + " - " + itemOrdered[i].getDirector()+ " - " + itemOrdered[i].getLength()+ " : " + itemOrdered[i].getCost() + "$");
        }
        System.out.println("Total cost: " + totalCost());
        System.out.println("*****************************************************************************************");
    }
}