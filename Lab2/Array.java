
import java.util.Scanner;
public class Array {
	public static void main(String[] args) {
		int []num;
		int sum=0;
		num = new int[10];
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Input size of array : ");
		int n = keyboard.nextInt();
		for(int i=0;i<n;i++) {
			System.out.printf("num[%d] = ",i);
			num[i] = keyboard.nextInt();
		}
		for(int i=0;i<n-1;i++) {
			for(int j=i;j<n;j++) {
				if(num[i]>num[j]) {
					int temp = num[i];
					num[i] = num[j];
					num[j] =  temp;
				}
			}
		}
		System.out.println("The array after resort:");
		for(int i=0;i<n;i++) {
			sum+=num[i];
			System.out.printf("num[%d] = %d\t",i,num[i]);
		}
		System.out.printf("The sum of array = %d\t",sum);
		System.out.printf("The average of array = %d\t",sum/n);
	}
}