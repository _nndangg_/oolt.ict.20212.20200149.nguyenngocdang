import java.io.*;
import java.util.*;
public class AddTwoMatrices {
	public static void main(String[] args) {
		int matA[][] = { { 3, 10, 5, 1 }, { 7, 2, 9, 1 }, { 2, 3, 6, 3 } };
		int matB[][] = { { 2, 2, 3, 4 }, { 5, 6, 8, 8 }, { 2, 1, 3, 12 } };
		int matC[][] =new int[3][4];
		int i,j;
		Scanner keyboard = new Scanner(System.in);
		for(i=0;i<matA.length;i++) {
			for(j=0;j<matA[i].length;j++) {
				matC[i][j] = matA[i][j] + matB[i][j];
			}
		}
		System.out.printf("The sum of 2 matrices = \n");
		for(i=0;i<matA.length;i++) {
			for(j=0;j<matA[i].length;j++) {
				System.out.printf("%d\t",matC[i][j]);
			}
			System.out.printf("\n");
		}
	}
}