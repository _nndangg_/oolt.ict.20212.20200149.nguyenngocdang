import java.util.Scanner;
public class DayOfMonth {
	public static void main(String args[]){
	Scanner keyboard = new Scanner(System.in);
	
	System.out.println("What's the month?");
	String strMonth = keyboard.nextLine();
	System.out.println("What's the year ?");
	int iYear = keyboard.nextInt();
	
	
	if((strMonth.compareTo("January")==0)||(strMonth.compareTo("Jan")==0)||(strMonth.compareTo("Jan.")==0)||(strMonth.compareTo("1")==0)) {
		System.out.println("The month "+strMonth+ " in "+iYear+" has 31 days");
	}
	else if((strMonth.compareTo("February")==0)||(strMonth.compareTo("Feb.")==0)||(strMonth.compareTo("Feb")==0)||(strMonth.compareTo("2")==0)) {
		if ( (iYear % 400 ==0) || ( iYear % 4 ==0) && (iYear % 100 !=0)){
		System.out.println("The month "+strMonth+ " in "+iYear+" has 28 days");
		}
		else {
			System.out.println("The month "+strMonth+ " in "+iYear+" has 29 days");
		}
	}
	else if((strMonth.compareTo("March")==0)||(strMonth.compareTo("Mar")==0)||(strMonth.compareTo("Mar.")==0)||(strMonth.compareTo("3")==0)) {
		System.out.println("The month "+strMonth+ " in "+iYear+" has 31 days");
	}
	else if((strMonth.compareTo("April")==0)||(strMonth.compareTo("Apr")==0)||(strMonth.compareTo("Apr.")==0)||(strMonth.compareTo("4")==0)) {
		System.out.println("The month "+strMonth+ " in "+iYear+" has 30 days");
	}
	else if((strMonth.compareTo("May")==0)||(strMonth.compareTo("May")==0)||(strMonth.compareTo("May.")==0)||(strMonth.compareTo("5")==0)) {
		System.out.println("The month "+strMonth+ " in "+iYear+" has 31 days");
	}
	else if((strMonth.compareTo("June")==0)||(strMonth.compareTo("Jun.")==0)||(strMonth.compareTo("Jun")==0)||(strMonth.compareTo("6")==0)) {
		System.out.println("The month "+strMonth+ " in "+iYear+" has 30 days");
	}
	else if((strMonth.compareTo("July")==0)||(strMonth.compareTo("Jul.")==0)||(strMonth.compareTo("Jul")==0)||(strMonth.compareTo("7")==0)) {
		System.out.println("The month "+strMonth+ " in "+iYear+" has 31 days");
	}
	else if((strMonth.compareTo("August")==0)||(strMonth.compareTo("Aug.")==0)||(strMonth.compareTo("Aug")==0)||(strMonth.compareTo("8")==0)) {
		System.out.println("The month "+strMonth+ " in "+iYear+" has 31 days");
	}
	else if((strMonth.compareTo("September")==0)||(strMonth.compareTo("Sept.")==0)||(strMonth.compareTo("Sep")==0)||(strMonth.compareTo("9")==0)) {
		System.out.println("The month "+strMonth+ " in "+iYear+" has 30 days");
	}
	else if((strMonth.compareTo("October")==0)||(strMonth.compareTo("Oct.")==0)||(strMonth.compareTo("Oct")==0)||(strMonth.compareTo("10")==0)) {
		System.out.println("The month "+strMonth+ " in "+iYear+" has 31 days");
	}
	else if((strMonth.compareTo("November")==0)||(strMonth.compareTo("Nov")==0)||(strMonth.compareTo("Nov.")==0)||(strMonth.compareTo("11")==0)) {
		System.out.println("The month "+strMonth+ " in "+iYear+" has 30 days");
	}
	else if((strMonth.compareTo("December")==0)||(strMonth.compareTo("Dec")==0)||(strMonth.compareTo("Dec.")==0)||(strMonth.compareTo("12")==0)) {
		System.out.println("The month "+strMonth+ " in "+iYear+" has 31 days");
	}
	else {
		System.out.println("Your input is invalid\n");
	}
	
	}
}
