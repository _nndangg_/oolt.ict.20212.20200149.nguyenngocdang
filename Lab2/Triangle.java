import java.util.Scanner;
public class Triangle {
	public static void main(String args[]) {
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("What is the height of triangle ?");
		int n = keyboard.nextInt();
		for(int i=1;i<=n;i++) {
			for(int j=1;j<=2*n-1;j++) {
				if((j<=n-i)||(j>n-1+i)) {
					System.out.print(" ");
				}
				else {
					System.out.print("*");
				}
			}
			System.out.print("\n");
			
		}
	}

}
